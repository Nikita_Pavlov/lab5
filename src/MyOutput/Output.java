package MyOutput;

/**
 * интерфейс классов вывода
 */
public interface Output {
    void print(String message);

    void println(String message);

    void println();
}
