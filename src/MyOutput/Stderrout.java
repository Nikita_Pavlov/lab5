package MyOutput;

/**
 * класс вывода ошибок в консоль
 */
public class Stderrout implements Output{
    @Override
    public void print(String message) {
        System.err.print(message);
    }

    @Override
    public void println(String message) {
        System.err.println(message);
    }

    @Override
    public void println() {
        System.err.println();
    }
}
