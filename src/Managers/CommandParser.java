package Managers;

import Exceptions.NoSuchCommandException;
import MyOutput.Output;
import MyOutput.Stderrout;

import java.util.Arrays;

/**
 * парсер команд, скармливет результат менджеру команд CommandManager
 */

public class CommandParser {
    private CommandManager commandManager;
    Output errouter;

    public CommandParser(CommandManager commandManager){
        this.commandManager = commandManager;
        this.errouter = new Stderrout();
    }

    public CommandParser(CommandManager commandManager, Output errouter){
        this.commandManager = commandManager;
        this.errouter = errouter;
    }

    public void parseline(String redLine){
        String[] line = redLine.split(" ");
        String[] params;
        try{
            switch (line.length){
                case 1:
                    params = null;
                    break;
                case 2:
                    params = new String[]{line[1]};
                    break;
                default:
                    params = Arrays.copyOfRange(line, 1, line.length);
            }
            if(commandManager!=null)
                commandManager.execute(line[0], params);
            else
                System.out.println("commandManager not found");
        }
        catch (NoSuchCommandException e){
            errouter.println(e.getError_message());
        }
    }
}
