package Managers;

import Commands.*;
import MyOutput.Output;
import MyOutput.Stderrout;
import MyOutput.Stdout;
/**
 * инвокер, отвечающий за команды связанные с работой программы
 */
public class ProgramManager extends Invokers{
    private boolean dontExit = true;
    public CommandParser parser;

    {
        this.invokerName = "ProgramManager";
        cmdMap.put("exit", new Cmd_exit(this));
        cmdMap.put("help", new Cmd_help());
        cmdMap.put("execute_script",new Cmd_execute_script(parser));
    }

    public ProgramManager(Output outer, Output errout){
        this.outer = outer;
        this.errout = errout;
    }

    public ProgramManager(Output outer){
        this.outer = outer;
        this.errout = new Stderrout();
    }

    public ProgramManager(){
        this.outer = new Stdout();
        this.errout = new Stderrout();
    }

    public void setParser(CommandParser parser){
        this.parser = parser;
        cmdMap.put("execute_script",new Cmd_execute_script(parser));
    }

    public void exit(){
        dontExit = false;
    }

    public boolean dontExit() {
        return dontExit;
    }
}
