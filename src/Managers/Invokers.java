package Managers;

import Commands.Command;
import Exceptions.CantExecuteCommandException;
import Exceptions.WrongAmountofParametersExceptions;
import MyOutput.Output;

import java.util.HashMap;
import java.util.Map;
/**
 * Абстрактный класс-родитель всех инвокеров
 */

public abstract class Invokers{
    public Map<String, Command> cmdMap = new HashMap<>();
    protected Output outer;
    protected Output errout;
    protected String invokerName;
    private Command cmd;
    private int provided,amount;

    public void execute(String cmdName, String[] params)
            throws CantExecuteCommandException, WrongAmountofParametersExceptions {
        if (cmdMap.containsKey(cmdName)){
            cmd=cmdMap.get(cmdName);
            amount = cmd.getParamsAmount();
            if (params != null)
                provided = params.length;
            else provided = 0;
            if (provided == amount) {
                cmd.execute(params);
            }
            else throw new WrongAmountofParametersExceptions(cmdName,amount,provided);
        }
        else throw new CantExecuteCommandException(invokerName,cmdName);
    }
}