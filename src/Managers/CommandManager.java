package Managers;

import Exceptions.*;
import MyOutput.*;

import java.util.HashMap;
import java.util.Map;

/**
 * распределитель команд по инвокерам
 */
public class CommandManager{
    private Map<String, Invokers> invokersMap = new HashMap<>();
    private Output errouter;
    private CommandParser parser;
    private ProgramManager programManager;

    public void upd_programManager(ProgramManager programManager){
        invokersMap.put("exit", programManager);
        invokersMap.put("help", programManager);
        invokersMap.put("execute_script", programManager);
        programManager.parser = this.parser;
    }

    public void upd_collectionManager(CollectionManager collectionManager){
        invokersMap.put("show",collectionManager);
        invokersMap.put("info",collectionManager);
        invokersMap.put("add",collectionManager);
        invokersMap.put("addf",collectionManager);
        invokersMap.put("update",collectionManager);//todo
        invokersMap.put("remove_by_id",collectionManager);
        invokersMap.put("clear",collectionManager);
        invokersMap.put("save",collectionManager);//todo
        invokersMap.put("remove_first",collectionManager);
        invokersMap.put("remove_head",collectionManager);
        invokersMap.put("add_if_min",collectionManager);//todo
        invokersMap.put("filter_contains_name",collectionManager);
        invokersMap.put("print_descending",collectionManager);//todo
        invokersMap.put("print_field_descending_semester_enum",collectionManager);
    }
    /*
save : сохранить коллекцию в файл
 */
    public CommandManager(ProgramManager programManager, CollectionManager collectionManager){
        upd_programManager(programManager);
        upd_collectionManager(collectionManager);
        this.programManager=programManager;
        this.errouter = new Stderrout();
    }

    public CommandManager(ProgramManager programManager, CollectionManager collectionManager,  Output errouter){
        upd_programManager(programManager);
        upd_collectionManager(collectionManager);
        this.errouter=errouter;
    }

    public void setParser(CommandParser parser){
        this.parser = parser;
        programManager.setParser(parser);
    }

    public void execute(String cmdName, String[] params) throws NoSuchCommandException {
        if (invokersMap.containsKey(cmdName)){
            try{
                invokersMap.get(cmdName).execute(cmdName, params);
            }
            catch (CantExecuteCommandException e){
                errouter.println(e.getError_message());
            }
            catch (WrongAmountofParametersExceptions e){
                errouter.println(e.getError_message());
            }
        }
        else if (!cmdName.isEmpty()) throw new NoSuchCommandException(cmdName);
    }

}
