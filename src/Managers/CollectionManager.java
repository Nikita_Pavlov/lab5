package Managers;

import CollectionContent.*;
import Commands.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayDeque;

/**
 * инвокер, отвечающий за команды связанные с коллекцие и ее элементами
 */

public class CollectionManager extends Invokers{
    private ArrayDeque<StudyGroup> groups;
    private String[] collectionMetaData;
    private CommandParser parser;

    public CollectionManager(ArrayDeque<StudyGroup> groups, String[] collectionMetaData){
        this.groups = groups;
        this.collectionMetaData=collectionMetaData;
        cmdMap.put("show",new Cmd_show<>(groups));
        cmdMap.put("remove_first",new Cmd_remove_first<>(groups));
        cmdMap.put("remove_head", new Cmd_remove_head<>(groups));
        cmdMap.put("clear", new Cmd_clear<>(groups));
        cmdMap.put("info", new Cmd_info(collectionMetaData, groups));
        cmdMap.put("remove_by_id",new Cmd_remove_by_id(groups));
        cmdMap.put("filter_contains_name",new Cmd_filter_contains_name(groups));
        cmdMap.put("print_field_descending_semester_enum",new Cmd_print_field_descending_semester_enum(groups));
        cmdMap.put("print_descending",new Cmd_print_descending(groups));
        cmdMap.put("add",new Cmd_add(groups));
        cmdMap.put("addf",new Cmd_addf(groups));
        cmdMap.put("add_if_min",new Cmd_add_if_min(groups));
        cmdMap.put("update",new Cmd_update_id(groups));
        cmdMap.put("save",new Cmd_save(groups));
    }

    public void readAllFromFile(String filepath){
        try (BufferedReader cmndReader = new BufferedReader(new FileReader(filepath))) {
            String line;
            cmndReader.readLine();
            while (cmndReader.ready()) {
                line="addf ";
                line += cmndReader.readLine().replace(","," ");
                parser.parseline(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setParser(CommandParser parser) {
        this.parser=parser;
    }
}