package Exceptions;

/**
 * исключение, возникающее, когда команде подается неверное количество параметров
 */
public class WrongAmountofParametersExceptions extends Exception{
    private final String error_message;

    public WrongAmountofParametersExceptions(String cmdName, int amount, int provided) {
        this.error_message="Command \""+cmdName+"\""+" has "+ amount + " parameters, but "+
                provided+" were provided";
    }

    public String getError_message(){
        return error_message;
    }
}
