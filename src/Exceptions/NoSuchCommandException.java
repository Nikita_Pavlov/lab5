package Exceptions;

/**
 * исключение, возникающее когда введенной команды нет в списке доступных
 */
public class NoSuchCommandException extends Exception{
    private final String error_message;

    public NoSuchCommandException(String cmdName) {
        this.error_message="Command \""+cmdName+"\""+" not found";
    }

    public String getError_message(){
        return error_message;
    }
}