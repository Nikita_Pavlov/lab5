package Exceptions;

/**
 * исключение, воозникающее когда инвокера пытаются заставить выполнить команду, которую он не может выполнять
 */
public class CantExecuteCommandException extends RuntimeException{
    private final String error_message = " can't execute command \"";
    private String invokerName;
    private String cmdName;

    public CantExecuteCommandException(String invokerName, String cmdName) {
        this.invokerName = invokerName;
        this.cmdName = cmdName;
    }

    public String getError_message(){
        return invokerName+error_message+cmdName+"\"";
    }
}
