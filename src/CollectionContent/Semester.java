package CollectionContent;

/**
 * энам семестра
 */
public enum Semester {
    FIRST("FIRST"),
    THIRD("THIRD"),
    FOURTH("FOURTH"),
    FIFTH("FIFTH");
    public final String semesterName;

    private Semester(String semesterName){
        this.semesterName = semesterName;
    }
}