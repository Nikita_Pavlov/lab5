package CollectionContent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
/**
 * класс человека
 */

public class Person {
    private String name; //Поле не может быть null, Строка не может быть пустой
    private java.time.LocalDate birthday; //Поле не может быть null
    private Float weight; //Поле может быть null, Значение поля должно быть больше 0
    private String passportID; //Значение этого поля должно быть уникальным, Поле может быть null
    private Color eyeColor; //Поле может быть null

    public Person(String name, LocalDate birthday, Float weight, String passportID, Color eyeColor) {
        this.name = name;
        this.birthday = birthday;
        this.weight = weight;
        this.passportID = passportID;
        this.eyeColor = eyeColor;
    }

    @Override
    public String toString(){
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return name +" "+ birthday.toString()+" "+weight+" "+passportID+" "+eyeColor;
    }

    public String getPassID() {
        return this.passportID;
    }
}