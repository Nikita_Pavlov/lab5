package CollectionContent;

/**
 * класс задающий структуру координат
 */
public class Coordinates {
    private long x; //Значение поля должно быть больше -587
    private Double y; //Поле не может быть null

    public Coordinates(long x, Double y) {
        this.x = x;
        this.y = y;
    }

    public long getX(){
        return x;
    }

    public Double getY(){
        return y;
    }

    @Override
    public String toString(){
        return x+" "+y;
    }
}