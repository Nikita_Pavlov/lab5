package CollectionContent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * класс элементов коллекции
 */
public class StudyGroup {
    private static Random random;
    static {
       random = new Random();
    }
    private int id; //Must be grater than 0. must be unique. Must be generated automatically
    private String name; //Поле не может быть null, Строка не может быть пустой
    private Coordinates coordinates; //Поле не может быть null
    private Date creationDate; //Must not be null. Must be generated automatically
    private long studentsCount; //Значение поля должно быть больше 0
    private Integer transferredStudents; //Значение поля должно быть больше 0, Поле может быть null
    private Integer averageMark; //Значение поля должно быть больше 0, Поле может быть null
    private Semester semesterEnum; //Поле не может быть null
    private Person groupAdmin; //Поле не может быть null

    public StudyGroup(String name, Coordinates coordinates, long studentsCount, Integer transferredStudents,
                      Integer averageMark, Semester semesterEnum, Person groupAdmin){
        this.id = StudyGroup.random.nextInt();
        this.name = name;
        this.coordinates = coordinates;
        this.creationDate = new Date(System.currentTimeMillis());
        this.studentsCount = studentsCount;
        this.transferredStudents = transferredStudents;
        this.averageMark = averageMark;
        this.semesterEnum = semesterEnum;
        this.groupAdmin = groupAdmin;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd;HH:mm:ss");
        return id+" "+name+" "+coordinates.toString()+" "+formatter.format(creationDate)+" "+studentsCount+" "+
                transferredStudents+" "+averageMark+" "+semesterEnum+" "+groupAdmin.toString();
    }

    public Semester getSemester() {
        return semesterEnum;
    }

    public long getStudentsCount() {
        return studentsCount;
    }

    public String getPassID() {
        return this.groupAdmin.getPassID();
    }

    public long getValue() {
        return this.getStudentsCount();
    }
}
