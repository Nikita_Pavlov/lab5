import CollectionContent.StudyGroup;
import Managers.*;
import MyOutput.Stderrout;
import MyOutput.Stdout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Date;

public class Main {
    public static void main(String[] args){
        ArrayDeque<StudyGroup> groups = new ArrayDeque<>();
        String[] collectionMetaData = new String[3];//init_date, type
        Format formatter = new SimpleDateFormat("yyyy-MM-dd;HH:mm:ss");
        collectionMetaData[0] = formatter.format(new Date(System.currentTimeMillis()));
        collectionMetaData[1] = "StudyGroup";

        Stdout outer = new Stdout();
        Stderrout errouter = new Stderrout();

        ProgramManager programManager = new ProgramManager();
        CollectionManager collectionManager = new CollectionManager(groups, collectionMetaData);
        CommandManager commandManager = new CommandManager(programManager, collectionManager);
        CommandParser parser = new CommandParser(commandManager);
        collectionManager.setParser(parser);
        commandManager.setParser(parser);

        //collectionManager.readAllFromFile(System.getenv().get("INIT_COL"));
        collectionManager.readAllFromFile("src/Scripts/initial_collection.txt");
        try (BufferedReader cmndReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (programManager.dontExit()) {
                parser.parseline(cmndReader.readLine());
            }
        } catch (IOException e) {
            errouter.println(e.getMessage());
        }
    }
}