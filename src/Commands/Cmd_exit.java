package Commands;

import Managers.ProgramManager;

/**
 * завершить программу (без сохранения в файл)
 */
public class Cmd_exit implements Command {
    ProgramManager programManager;
    public Cmd_exit(ProgramManager programManager){
        this.programManager = programManager;
    }

    @Override
    public void execute(String[] params) {
        programManager.exit();
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
