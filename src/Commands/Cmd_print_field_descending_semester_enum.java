package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stderrout;
import MyOutput.Stdout;

import java.util.*;

/**
 * вывести значения поля semesterEnum всех элементов в порядке убывания
 */

public class Cmd_print_field_descending_semester_enum implements Command{
    private ArrayDeque<StudyGroup> groups;
    private Output outer;
    private Output errouter;

    public Cmd_print_field_descending_semester_enum(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.outer = new Stdout();
        this.errouter = new Stderrout();
    }

    @Override
    public void execute(String[] params) {
        if (groups.size()==0){
            errouter.println("Error: the collection is already empty");
            return;
        }
        class Enumcount{
            public int amount;
            public String semesterName;

            public Enumcount(String semesterName){
                this.semesterName=semesterName;
            }
        }

        Enumcount[] enumcount = new Enumcount[4];
        enumcount[0]=new Enumcount("FIRST");
        enumcount[1]=new Enumcount("THIRD");
        enumcount[2]=new Enumcount("FOURTH");
        enumcount[3]=new Enumcount("FIFTH");

        for(StudyGroup group: groups){
            enumcount[group.getSemester().ordinal()].amount+=1;
        }

        Arrays.sort(enumcount, Comparator.comparingInt(o -> o.amount));

        for (Enumcount sem: enumcount){
            for (int k = 0; k < sem.amount; k++) {
                outer.println(sem.semesterName);
            }
        }
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
