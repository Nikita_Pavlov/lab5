package Commands;

import CollectionContent.*;
import MyOutput.Output;
import MyOutput.Stdout;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.Arrays;

/**
 * Добавить новый элемент в коллекцию, но при чтении команды из файла
 */
public class Cmd_addf implements Command{
    ArrayDeque<StudyGroup> groups;
    private Output errouter;
    private Output outer;

    public Cmd_addf(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.errouter = new Stdout();
        this.outer = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        StudyGroup group;
        String line;
        boolean errFlag=false;

        String name;
        name =  params[0];
        if (name.isEmpty()){
            errouter.println("Group's name must not be empty");
            errFlag=true;
        }

        int x=0;
        line =  params[1];
        try{
            x = Integer.parseInt(line);
            if (x<-587){
                errouter.println("x coordinate must be an integer from -587 to "+Integer.MAX_VALUE);
                errFlag=true;
            }
        }
        catch (Exception e){
            errouter.println("x coordinate must be an integer from -587 to "+Integer.MAX_VALUE);
            errFlag=true;
        }

        double y=0;
        line =  params[2];
        try{
            y = Double.parseDouble(line);
        }
        catch (Exception e){
            errouter.println("y coordinate must be a decimal number from "+Double.MIN_VALUE+
                    " to " + Double.MAX_VALUE);
            errFlag=true;
        }

        long studentsCount=0;
        line =  params[3];
        try{
            studentsCount = Long.parseLong(line);
            if (studentsCount<=0){
                errouter.println("studentsCount must be a positive integer not greater than "
                        + Long.MAX_VALUE);
                errFlag=true;
            }
        }
        catch (Exception e){
            errouter.println("studentsCount must be a positive integer not greater than " + Long.MAX_VALUE);
            errFlag=true;
        }

        Integer transferredStudents=null;
        line =  params[4];
        if (!(line==null || line.isEmpty())){
            try{
                transferredStudents = Integer.valueOf(line);
                if (transferredStudents<=0){
                    errouter.println("studentsCount must be a positive integer not greater than "
                            + Integer.MAX_VALUE+" or null");
                    errFlag=true;
                }
            }
            catch (Exception e){
                errouter.println("studentsCount must be a positive integer not greater than " + Integer.MAX_VALUE+
                        " or null");
                errFlag=true;
            }
        }


        Integer averageMark=null;
        line =  params[5];
        if (!(line==null || line.isEmpty())) {
            try {
                averageMark = Integer.valueOf(line);
            } catch (Exception e) {
                errouter.println("averageMark must be a positive integer not greater than " + Integer.MAX_VALUE +
                        " or null");
                errFlag=true;
            }
        }

        Semester semesterEnum=null;
        line =  params[6];
        switch (line){
            case "FIRST":
                semesterEnum=Semester.FIRST;
                break;
            case "THIRD":
                semesterEnum=Semester.THIRD;
                break;
            case "FOURTH":
                semesterEnum=Semester.FOURTH;
                break;
            case "FIFTH":
                semesterEnum=Semester.FIFTH;
                break;
            default:
                errouter.println("semesterEnum must be one of these values: FIRST/THIRD/FOURTH/FIFTH");
                errFlag=true;
        }

        String aname;
        aname =  params[7];
        if (aname.isEmpty()){
            errouter.println("Admin's name must not be empty");
            errFlag=true;
        }

        LocalDate birthday=null;
        line =  params[8];
        try{
            birthday = LocalDate.parse(line);
        }
        catch (Exception e){
            errouter.println("Admin's birthday must be in format YYYY-MM-DD");
            errFlag=true;
        }

        Float weight = null;
        line =  params[9];
        if (!(line==null || line.isEmpty())) {
            try {
                weight = Float.valueOf(line);
                if (weight <= 0){
                    errouter.println("weight must be a positive decimal number not greater than " + Float.MAX_VALUE);
                    errFlag=true;
                }
            } catch (Exception e) {
                errouter.println("weight must be a positive decimal number not greater than " + Float.MAX_VALUE);
                errFlag=true;
            }
        }

        String passportID=null;
        line =  params[10];
        if (!(line==null || line.isEmpty())) {
            passportID = line;
            String[] passes = new String[groups.size()];
            int kk = 0;
            for (StudyGroup groupp : groups) {
                passes[kk] = groupp.getPassID();
                kk += 1;
            }
            if (Arrays.asList(passes).contains(passportID)) {
                errouter.println("passportID must be unique");
                errFlag=true;
            }
        }

        Color eyeColor=null;
        line =  params[11];
        if (!(line==null || line.isEmpty())) {
            switch (line) {
                case "BLACK":
                    eyeColor = Color.BLACK;
                    break;
                case "YELLOW":
                    eyeColor = Color.YELLOW;
                    break;
                case "ORANGE":
                    eyeColor = Color.ORANGE;
                    break;
                case "WHITE":
                    eyeColor = Color.WHITE;
                    break;
                case "BROWN":
                    eyeColor = Color.BROWN;
                    break;
                default:
                    errouter.println("eyeColor must be one of these values: BLACK/YELLOW/ORANGE/WHITE/BROWN");
                    errFlag=true;
            }
        }

        if (!errFlag) {
            Coordinates coordinates = new Coordinates(x, y);

            Person groupAdmin = new Person(aname, birthday, weight, passportID, eyeColor);
            group = new StudyGroup(name, coordinates, studentsCount, transferredStudents,
                    averageMark, semesterEnum, groupAdmin);
            groups.addLast(group);
        }
    }

    @Override
    public int getParamsAmount() {
        return 12;
    }

}
