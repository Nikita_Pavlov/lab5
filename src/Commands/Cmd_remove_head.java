package Commands;

import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;

/**
 *вывести первый элемент коллекции и удалить его
 */

public class Cmd_remove_head<T> implements Command{
    private ArrayDeque<T> groups;
    Output errouter;

    public Cmd_remove_head(ArrayDeque<T> groups){
        this.groups=groups;
        this.errouter = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        if (groups.size()==0){
            errouter.println("Error: the collection is already empty");
            return;
        }
        ArrayDeque<T> tmpAd = new ArrayDeque<>();
        tmpAd.addFirst(groups.pollFirst());
        Cmd_show<T> tmpCmd = new Cmd_show<>(tmpAd);
        tmpCmd.execute(null);
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
