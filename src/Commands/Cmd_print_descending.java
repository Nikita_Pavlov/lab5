package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;

/**
 * вывести элементы коллекции в порядке убывания
 */

public class Cmd_print_descending implements Command{
    ArrayDeque<StudyGroup> groups;
    private Output outer;

    public Cmd_print_descending(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.outer=new Stdout();
    }

    @Override
    public void execute(String[] params) {
        StudyGroup[] groupsArr = groups.toArray(new StudyGroup[0]);
        Arrays.sort(groupsArr, Comparator.comparingLong(StudyGroup::getStudentsCount).reversed());
        ArrayDeque<StudyGroup> sortedgroups = new ArrayDeque<>();

        for (StudyGroup group: groupsArr){
            sortedgroups.addLast(group);
        }

        Cmd_show<StudyGroup> tmpCmd = new Cmd_show<>(sortedgroups);
        tmpCmd.execute(null);
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
