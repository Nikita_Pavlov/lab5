package Commands;

import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;
import java.util.Iterator;

/**
 * вывести в стандартный поток вывода все элементы коллекции в строковом представлении
 */

public class Cmd_show<T> implements Command {
    private ArrayDeque<T> groups;
    private Output outer;
    private String[] columnTitles = {"id", "name", "x", "y", "creationDate", "studentsCount", "transferredStudents",
            "averageMark", "semesterEnum", "adminName", "birthday", "wight", "passportID", "eyeColor"};

    public Cmd_show(ArrayDeque<T> groups) {
        this.groups = groups;
        this.outer = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        if (groups.size() == 0) {
            outer.println("the collection is empty");
            return;
        }
        Iterator<T> it = groups.iterator();
        int arrlen = columnTitles.length;
        int[] maxlens = new int[arrlen];
        int len;

        for (int i = 0; i < arrlen; i++) {
            maxlens[i] = columnTitles[i].length();
        }
        String[] redstr;
        while (it.hasNext()) {
            redstr = it.next().toString().split(" ");
            for (int i = 0; i < arrlen; i++) {
                len = redstr[i].length();
                if (maxlens[i] < len) maxlens[i] = len;
            }
        }
        for (int i = 0; i < arrlen; i++) {
            outer.print(columnTitles[i]);
            for (int k = 0; k <= maxlens[i] - columnTitles[i].length(); k++) {
                outer.print(" ");
            }
        }
        outer.println();
        it = groups.iterator();
        while (it.hasNext()) {
            redstr = it.next().toString().split(" ");
            for (int i = 0; i < arrlen; i++) {
                outer.print(redstr[i]);
                for (int k = 0; k <= maxlens[i] - redstr[i].length(); k++) {
                    outer.print(" ");
                }
            }
            outer.println();
        }
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
