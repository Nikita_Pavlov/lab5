package Commands;

import MyOutput.Output;
import MyOutput.Stdout;

/**
 * вывести справку по доступным командам
 */

public class Cmd_help implements Command {
    Output outer;
    public Cmd_help(Output outer){
        this.outer = outer;
    }

    public Cmd_help(){
        this.outer = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        outer.print("help : вывести справку по доступным командам\n" +
                "info : вывести в стандартный поток вывода информацию о коллекции (тип, дата инициализации, количество элементов и т.д.)\n" +
                "show : вывести в стандартный поток вывода все элементы коллекции в строковом представлении\n" +
                "add {element} : добавить новый элемент в коллекцию\n" +
                "update id {element} : обновить значение элемента коллекции, id которого равен заданному\n" +
                "remove_by_id id : удалить элемент из коллекции по его id\n" +
                "clear : очистить коллекцию\n" +
                "save : сохранить коллекцию в файл\n" +
                "execute_script file_name : считать и исполнить скрипт из указанного файла. В скрипте содержатся команды в таком же виде, в котором их вводит пользователь в интерактивном режиме.\n" +
                "exit : завершить программу (без сохранения в файл)\n" +
                "remove_first : удалить первый элемент из коллекции\n" +
                "remove_head : вывести первый элемент коллекции и удалить его\n" +
                "add_if_min {element} : добавить новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции\n" +
                "filter_contains_name name : вывести элементы, значение поля name которых содержит заданную подстроку\n" +
                "print_descending : вывести элементы коллекции в порядке убывания\n" +
                "print_field_descending_semester_enum : вывести значения поля semesterEnum всех элементов в порядке убывания\n");
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
