package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;

/**
 * вывести в стандартный поток вывода информацию о коллекции (тип, дата инициализации, количество элементов)
 */

public class Cmd_info implements Command{
    private String[] collectionMetaData;
    private ArrayDeque<StudyGroup> groups;
    private Output outer;

    public Cmd_info(String[] collectionMetaData, ArrayDeque<StudyGroup> groups){
        this.outer=new Stdout();
        this.collectionMetaData=collectionMetaData;
        this.groups=groups;
    }

    @Override
    public void execute(String[] params) {
        outer.println("creationDate type size");
        outer.println(collectionMetaData[0]+" "+collectionMetaData[1]+" "+groups.size());
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
