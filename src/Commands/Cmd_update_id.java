package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stderrout;
import MyOutput.Stdout;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Iterator;

/**
 * обновить значение элемента коллекции, id которого равен заданному
 */

public class Cmd_update_id implements Command{
    ArrayDeque<StudyGroup> groups;
    Output errouter;

    public Cmd_update_id(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.errouter=new Stderrout();
    }

    @Override
    public void execute(String[] params) {
        String[] ids = new String[groups.size()];
        int kk=0;
        for(StudyGroup groupp: groups){
            ids[kk]=Integer.toString(groupp.getId());
            kk+=1;
        }
        if (!Arrays.asList(ids).contains(params[0])){
            errouter.println("No element with such ID was found");
            return;
        }

        Cmd_add tmpCmd2 = new Cmd_add(groups);
        tmpCmd2.execute(null);

        ArrayDeque<StudyGroup> tmp_groups = new ArrayDeque<>();

        Iterator<StudyGroup> it = groups.iterator();
        StudyGroup group;

        int ifinal=groups.size();
        for (int i=0;i<ifinal-1;i++){
            group = groups.pollFirst();
            if (group.getId()!=Integer.parseInt(params[0])){
                tmp_groups.addLast(group);
            }
            else tmp_groups.addLast(groups.pollLast());
        }

        groups.addAll(tmp_groups);
    }

    @Override
    public int getParamsAmount() {
        return 1;
    }
}
