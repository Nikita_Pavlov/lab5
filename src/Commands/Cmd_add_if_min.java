package Commands;

import CollectionContent.StudyGroup;

import java.util.ArrayDeque;

/**
 * Добавить новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции
 */
public class Cmd_add_if_min implements Command{
    private ArrayDeque<StudyGroup> groups;

    public Cmd_add_if_min(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
    }

    @Override
    public void execute(String[] params) {
        Cmd_add tmpCmd = new Cmd_add(groups);
        long minValue = Long.MAX_VALUE;
        int kk=0;
        for(StudyGroup groupp: groups){
            if (kk== groups.size()-1) break;
            if(groupp.getValue()<minValue)
                minValue= groupp.getValue();
            kk+=1;
        }
        if(groups.peekLast().getValue()>=minValue){
            groups.pollLast();
        }
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
