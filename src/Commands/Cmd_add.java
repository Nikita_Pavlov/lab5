package Commands;

import CollectionContent.*;
import MyOutput.Output;
import MyOutput.Stdout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.Arrays;

/**
 * Добавить новый элемент в коллекцию
 */
public class Cmd_add implements Command{
    ArrayDeque<StudyGroup> groups;
    private Output errouter;
    private Output outer;

    public Cmd_add(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.errouter = new Stdout();
        this.outer = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        StudyGroup group;
        String line;
        BufferedReader cmndReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            outer.print("Enter group's name: ");
            String name;
            while (true){
                name =  cmndReader.readLine();
                if (!name.isEmpty()) break;
                else errouter.println("Group's name must not be empty");
            }

            outer.print("Enter x coordinate: ");
            int x;
            while (true){
                line =  cmndReader.readLine();
                try{
                    x = Integer.parseInt(line);
                    if (x>=-587) break;
                    else errouter.println("x coordinate must be an integer from -587 to "+Integer.MAX_VALUE);
                }
                catch (Exception e){
                    errouter.println("x coordinate must be an integer from -587 to "+Integer.MAX_VALUE);
                }
            }

            outer.print("Enter y coordinate: ");
            double y;
            while (true){
                line =  cmndReader.readLine();
                try{
                    y = Double.parseDouble(line);
                }
                catch (Exception e){
                    errouter.println("y coordinate must be a decimal number from "+Double.MIN_VALUE+
                            " to " + Double.MAX_VALUE);
                    continue;
                }
                break;
            }

            outer.print("Enter studentsCount: ");
            long studentsCount;
            while (true){
                line =  cmndReader.readLine();
                try{
                    studentsCount = Long.parseLong(line);
                    if (studentsCount>0) break;
                    else errouter.println("studentsCount must be a positive integer not greater than "
                                                                                        + Long.MAX_VALUE);
                }
                catch (Exception e){
                    errouter.println("studentsCount must be a positive integer not greater than " + Long.MAX_VALUE);
                }
            }

            outer.print("Enter transferredStudents: ");
            Integer transferredStudents=null;
            while (true){
                line =  cmndReader.readLine();
                if (line==null || line.isEmpty()) break;
                try{
                    transferredStudents = Integer.valueOf(line);
                    if (transferredStudents>0) break;
                    else errouter.println("studentsCount must be a positive integer not greater than "
                                                                                 + Integer.MAX_VALUE+" or null");
                }
                catch (Exception e){
                    errouter.println("studentsCount must be a positive integer not greater than " + Integer.MAX_VALUE+
                                    " or null");
                }
            }

            outer.print("Enter averageMark: ");
            Integer averageMark=null;
            while (true){
                line =  cmndReader.readLine();
                if (line==null || line.isEmpty()) break;
                try{
                    averageMark = Integer.valueOf(line);
                }
                catch (Exception e){
                    errouter.println("averageMark must be a positive integer not greater than " + Integer.MAX_VALUE+
                            " or null");
                    continue;
                }
                break;
            }

            outer.print("Enter semesterEnum FIRST/THIRD/FOURTH/FIFTH: ");
            Semester semesterEnum;
            while (true){
                line =  cmndReader.readLine();
                switch (line){
                    case "FIRST":
                        semesterEnum=Semester.FIRST;
                        break;
                    case "THIRD":
                        semesterEnum=Semester.THIRD;
                        break;
                    case "FOURTH":
                        semesterEnum=Semester.FOURTH;
                        break;
                    case "FIFTH":
                        semesterEnum=Semester.FIFTH;
                        break;
                    default:
                        errouter.println("semesterEnum must be one of these values: FIRST/THIRD/FOURTH/FIFTH");
                        continue;
                }
                break;
            }

            outer.print("Enter admin's name: ");
            String aname;
            while (true){
                aname =  cmndReader.readLine();
                if (!aname.isEmpty()) break;
                else errouter.println("Admin's name must not be empty");
            }

            outer.print("Enter admin's birthday in format YYYY-MM-DD: ");
            LocalDate birthday;
            while (true){
                line =  cmndReader.readLine();
                try{
                    birthday = LocalDate.parse(line);
                }
                catch (Exception e){
                    errouter.println("Admin's birthday must be in format YYYY-MM-DD");
                    continue;
                }
                break;
            }

            outer.print("Enter wieght: ");
            Float weight = null;
            while (true){
                line =  cmndReader.readLine();
                if (line==null || line.isEmpty()) break;
                try{
                    weight = Float.valueOf(line);
                    if (weight>0) break;
                    else errouter.println("weight must be a positive decimal number not greater than "+Float.MAX_VALUE);
                }
                catch (Exception e){
                    errouter.println("weight must be a positive decimal number not greater than "+Float.MAX_VALUE);
                }
            }

            outer.print("Enter passportID: ");
            String passportID=null;
            while (true){
                line =  cmndReader.readLine();
                if (line==null || line.isEmpty()) break;
                passportID=line;

                String[] passes = new String[groups.size()];
                int kk=0;
                for(StudyGroup groupp: groups){
                    passes[kk]=groupp.getPassID();
                    kk+=1;
                }
                if (Arrays.asList(passes).contains(passportID)){
                    errouter.println("passportID must be unique");
                    continue;
                }
                break;
            }

            outer.print("Enter eyeColor BLACK/YELLOW/ORANGE/WHITE/BROWN: ");
            Color eyeColor=null;
            while (true){
                line =  cmndReader.readLine();
                if (line==null || line.isEmpty()) break;
                switch (line){
                    case "BLACK":
                        eyeColor=Color.BLACK;
                        break;
                    case "YELLOW":
                        eyeColor=Color.YELLOW;
                        break;
                    case "ORANGE":
                        eyeColor=Color.ORANGE;
                        break;
                    case "WHITE":
                        eyeColor=Color.WHITE;
                        break;
                    case "BROWN":
                        eyeColor=Color.BROWN;
                        break;
                    default:
                        errouter.println("eyeColor must be one of these values: BLACK/YELLOW/ORANGE/WHITE/BROWN");
                        continue;
                }
                break;
            }

            Coordinates coordinates = new Coordinates(x, y);

            Person groupAdmin = new Person(aname, birthday, weight, passportID, eyeColor);
            group = new StudyGroup(name, coordinates, studentsCount, transferredStudents,
                    averageMark, semesterEnum, groupAdmin);
            groups.addLast(group);

        } catch (IOException e) {
            errouter.println(e.getMessage());
        }
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
