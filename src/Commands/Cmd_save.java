package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stdout;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayDeque;
import java.util.Random;

/**
 * сохранить коллекцию в файл
 */

public class Cmd_save implements Command {
    ArrayDeque<StudyGroup> groups;
    Output errouter;

    private static Random random;
    static {
        random = new Random();
    }

    public Cmd_save(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.errouter=new Stdout();
    }

    @Override
    public void execute(String[] params) {
        String outputFile = random.nextInt()+".csv";
        String text="id,name,x,y,creationDate,studentsCount,transferredStudents,averageMark," +
                "semesterEnum,adminName,birthday,wight,passportID,eyeColor\n";
        try {
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile));

            for(StudyGroup group: groups){
                text+=group.toString().replace(" ",",")+"\n";
            }
            byte[] buffer = text.getBytes();
            out.write(buffer, 0, buffer.length);
            out.close();
        }
        catch (Exception e){
            errouter.println(e.getMessage());
        }
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }
}
