package Commands;

import CollectionContent.StudyGroup;

import java.util.ArrayDeque;
import java.util.Iterator;

/**
 * вывести элементы, значение поля name которых содержит заданную подстроку
 */
public class Cmd_filter_contains_name implements Command{
    private ArrayDeque<StudyGroup> groups;

    public Cmd_filter_contains_name(ArrayDeque<StudyGroup> groups){
            this.groups=groups;
    }

    @Override
    public void execute(String[] params) {
        ArrayDeque<StudyGroup> newgroups = new ArrayDeque<>();
        Iterator<StudyGroup> it = groups.iterator();
        StudyGroup group;
        while (it.hasNext()) {
            group = it.next();
            if (group.getName().contains(params[0])){
                newgroups.addLast(group);
            }
        }
        Cmd_show<StudyGroup> tmpCmd = new Cmd_show<>(newgroups);
        tmpCmd.execute(null);
    }

    @Override
    public int getParamsAmount() {
        return 1;
    }

}
