package Commands;

import CollectionContent.StudyGroup;
import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Iterator;

/**
 * удалить элемент из коллекции по его id
 */

public class Cmd_remove_by_id implements Command{
    private ArrayDeque<StudyGroup> groups;
    Output errouter;

    public Cmd_remove_by_id(ArrayDeque<StudyGroup> groups){
        this.groups=groups;
        this.errouter = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        if (groups.size()==0){
            errouter.println("Error: the collection is already empty");
            return;
        }
        String[] ids = new String[groups.size()];
        int kk=0;
        for(StudyGroup groupp: groups){
            ids[kk]=Integer.toString(groupp.getId());
            kk+=1;
        }
        if (!Arrays.asList(ids).contains(params[0])){
            errouter.println("No element with such ID was found");
            return;
        }


        ArrayDeque<StudyGroup> newgroups = new ArrayDeque<>();
        Iterator<StudyGroup> it = groups.iterator();
        StudyGroup group;

        while (it.hasNext()) {
            it.next();
            group = groups.pollFirst();
            if (group.getId()!=Integer.parseInt(params[0])){
                newgroups.addLast(group);
            }
        }
        groups.addAll(newgroups);
    }

    @Override
    public int getParamsAmount() {
        return 1;
    }

}
