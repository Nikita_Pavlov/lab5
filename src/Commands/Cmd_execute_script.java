package Commands;

import Managers.CommandParser;
import MyOutput.Output;
import MyOutput.Stderrout;

import java.io.*;
import java.util.Arrays;

/**
 * считать и исполнить скрипт из указанного файла.
 * В скрипте содержатся команды в таком же виде, в котором их вводит пользователь в интерактивном режиме.
 * Выолнить скрипт из скрипта нельзя
 */

public class Cmd_execute_script implements Command{
    private String script_path;
    private Output errouter;
    private String line;
    private CommandParser parser;
    private static final String[] source_dependent = {"add","update_id","add_if_min"};
    private String[] params = new String[12];

    public Cmd_execute_script(CommandParser parser, Output errouter){
        this.errouter = errouter;
        this.parser = parser;
    }
    public Cmd_execute_script(CommandParser parser){
        this.errouter = new Stderrout();
        this.parser = parser;
    }

    @Override
    public void execute(String[] params) {
        if(params[0].endsWith(".txt"))
            script_path ="src/Scripts/"+params[0];
        else
            script_path ="src/Scripts/"+params[0]+".txt";

        try(BufferedReader cmndReader = new BufferedReader(new FileReader(script_path))) {
            while (cmndReader.ready()) {
                line = cmndReader.readLine();
                if (Arrays.asList(source_dependent).contains(line)){
                    line+="f";
                    for(int i=0;i<12;i++)
                        line+=" "+cmndReader.readLine();
                }
                if (line.length()>=14){
                    if (line.substring(0,14).equals("execute_script")){
                        errouter.println("Executing script from a script is not allowed");
                    }
                    else{
                        parser.parseline(line);
                    }
                }
                else{
                    parser.parseline(line);
                }
            }
        }
        catch(Exception e){
            errouter.println(e.getMessage());
        }
    }

    @Override
    public int getParamsAmount() {
        return 1;
    }

}