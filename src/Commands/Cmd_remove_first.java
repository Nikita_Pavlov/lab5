package Commands;

import MyOutput.Output;
import MyOutput.Stdout;

import java.util.ArrayDeque;

/**
 *удалить первый элемент из коллекции
 */

public class Cmd_remove_first<T> implements Command{
    private ArrayDeque<T> groups;
    Output errouter;

    public Cmd_remove_first(ArrayDeque<T> groups){
        this.groups=groups;
        this.errouter = new Stdout();
    }

    @Override
    public void execute(String[] params) {
        if (groups.size()==0){
            errouter.println("Error: the collection is already empty");
            return;
        }
        groups.pollFirst();
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
