package Commands;

/**
 * интерфейс всех коммнад
 */
public interface Command{
    void execute(String[] params);
    int getParamsAmount();
}
