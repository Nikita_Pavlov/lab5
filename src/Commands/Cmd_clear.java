package Commands;

import java.util.ArrayDeque;

/**
 *Очистить коллекцию
 */
public class Cmd_clear<T> implements Command{
    private ArrayDeque<T> groups;

    public Cmd_clear(ArrayDeque<T> groups){
        this.groups = groups;
    }

    @Override
    public void execute(String[] params) {
        groups.clear();
    }

    @Override
    public int getParamsAmount() {
        return 0;
    }

}
